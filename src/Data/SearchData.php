<?php

namespace App\Data;

class SearchData
{

    /**
     * @var string
     */
    public $q = "";

    /**
     * @var Diplome[]
     */
    public $diplome = [];

    /**
     * @var Secteur[]
     */
    public $secteur = [];

    /**
     * @var Contrat[]
     */
    public $contrat = [];

    /**
     * @var Departement[]
     */
    public $departement = [];
}