<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Etudiant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Etudiant>
 *
 * @method Etudiant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Etudiant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Etudiant[]    findAll()
 * @method Etudiant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EtudiantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Etudiant::class);
    }

    public function add(Etudiant $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Etudiant $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Etudiant[] Returns an array of Etudiant objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Etudiant
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    /**
     * Recuperer les resultat des requetes
     * @return Etudiant[]
     */
    public function findSearch(SearchData $search): array
    {
        $query = $this->createQueryBuilder('e')
            ->select('d', 'e')->join('e.diplome', 'd');
        if(!empty($search->diplome)){
            $query
            ->andWhere("e.diplome IN(:diplome)")->setParameter('diplome', $search->diplome);
        }
        if(!empty($search->secteur)){
            $query
                ->andWhere("e.secteur IN(:secteur)")->setParameter('secteur', $search->secteur);
        }
        if(!empty($search->contrat)){
            $query
                ->andWhere("e.contrat IN(:contrat)")->setParameter('contrat', $search->contrat);
        }
        if(!empty($search->departement)){
            $query
                ->andWhere("e.departement IN(:departement)")->setParameter('departement', $search->departement);
        }

        return $query->getQuery()->getResult();
        //return $this->findAll();
    }
}
