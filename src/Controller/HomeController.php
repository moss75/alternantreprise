<?php

namespace App\Controller;

use App\Entity\Etudiant;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $etudient = $entityManager->getRepository(Etudiant::class)->findAll();



        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'etudients' => $etudient,
        ]);
    }

//    /**
//     * @Route("/email", name="email")
//     */
//    public function email()
//    {
//        return $this->render('emails/activation.html.twig');
//
//    }


}
