<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Calendrier;
use App\Entity\Etudiant;
use App\Form\EtudiantFormType;
use App\Form\SearchForm;
use App\Repository\CalendrierRepository;
use App\Repository\EtudiantRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use mysql_xdevapi\Exception;
use PHPUnit\Framework\Error\Error;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class StudentController extends AbstractController
{
    /**
     * @Route("/student", name="student")
     */
    public function index(ManagerRegistry $doctrine, HttpFoundationRequest $request, SessionInterface $session, EtudiantRepository $etudiantRepository)
    {
        $entityManager = $doctrine->getManager();

        $data = new SearchData();
        $form = $this->createForm(SearchForm::class, $data);
        $form->handleRequest($request);
        $etudient = $entityManager->getRepository(Etudiant::class)->findSearch($data);


        $selection = $session->get('selection');
        $dataEtudiant = [];
        if($selection != null){
            foreach($selection as $id => $quantite){
                $etudiantId = $etudiantRepository->find($id);
                $dataEtudiant[] = [
                    "id" => $etudiantId->getId(),
                ];
            }
        }
        return $this->render('student/index.html.twig', [
            'controller_name' => 'StudentController',
            'etudients'  => $etudient,
            'form' => $form->createView(),
            'selection' => $dataEtudiant
        ]);
    }

    /**
     * @Route("/new_student", name="new_student")
     */
    public function newStudent(HttpFoundationRequest$request, EntityManagerInterface $entityManager, MailerInterface $mailer, CalendrierRepository $calendrierRepository)
    {
        try {
            $Allcalendrier = $calendrierRepository->findAll();
            $arrayCalendrier = [];
            foreach ($Allcalendrier as $date){
                $arrayCalendrier[] = $date->getStart()->format('Y-m-d H:i:s');
            }
            $etudiant = new Etudiant();
            $calendrier = new Calendrier();
            $form = $this->createForm(EtudiantFormType::class, $etudiant);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()){
                $dateInserer = $form->get('date')->getData();
                $today = new DateTime('now');
                $calendrier->setTitle("Inscription de ". $form->getData()->getPrenom()." ". $form->getData()->getNom());
                $calendrier->setDescription(
                    'Prenom : '. $form->getData()->getPrenom()."\n 
                Nom: ".$form->getData()->getNom()."\n 
                Age: ".$form->getData()->getAge()."\n
                Email: ".$form->getData()->getEmail()."\n
                Diplome: ".$form->getData()->getDiplome()->getLibelle()."\n
                Secteur: ".$form->getData()->getSecteur()->getLibelle()
                );

                $calendrier->setStart($today);
                $strDateInserer = $today->format('Y-m-d H:i:s');
                $newDate = new \DateTime(date('Y-m-d H:i:s', strtotime($strDateInserer)));
                $calendrier->setEnd($newDate->add(new \DateInterval('PT30M')));
                $file = $form['cv']->getData();

                $fileName = $file->getClientOriginalName();
                $file->move($this->getParameter('kernel.project_dir').'/public/cv', $fileName);
                $etudiant->setCv($fileName);
                $entityManager->persist($etudiant);
                $entityManager->flush();
                $entityManager->persist($calendrier);
                $entityManager->flush();

                $this->addFlash('success', 'Votre inscription à bien été prise en compte, vous receverez un mail pour confirmer le rendez-vous');
                $message = (new TemplatedEmail())
                    ->from(new Address('contact@alternantreprise.com', 'Contact Alternantreprise'))
                    ->to($etudiant->getEmail())
                    ->subject('Veuillez Valider votre inscription')
                    ->htmlTemplate('emails/etudiantRdv.html.twig')
                    ->context([
                        'prenom' => $etudiant->getPrenom(),
                        'date' => $dateInserer
                    ])
                ;
                $mailer->send($message);
                $messageInterne = (new TemplatedEmail())
                    ->from(new Address('test@test.fr', 'Contact Alternantreprise'))
                    ->to('alternantreprise@outlook.fr')
                    ->subject('Notification d\'inscription etudiant')
                    ->htmlTemplate('emails/notification.html.twig')
                    ->context([
                        'prenom' => $etudiant->getPrenom(),
                        'nom' => $etudiant->getNom(),
                        'secteur' => $etudiant->getSecteur()->getLibelle(),
                        'diplome' => $etudiant->getDiplome()->getLibelle(),
                        'cv' => $etudiant->getCv(),
                        'date' => $dateInserer
                    ])
                ;
                $mailer->send($messageInterne);
                return $this->redirectToRoute('home');
            }
        }
        catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
            return $this->redirectToRoute('home');
        }




        return $this->render('student/new.html.twig', [
            'form' => $form->createView(),
            'arrayCalendrier' => $arrayCalendrier
        ]);
    }


    /**
     * @Route("/delete_student", name="delete_student")
     */
    public function deleteStudent()
    {
        return$this->render('etudiant/delete.html.twig');

    }
}
