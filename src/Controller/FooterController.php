<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FooterController extends AbstractController
{
    #[Route('/mention-legale', name: 'mention-legale')]
    public function index(): Response
    {
        return $this->render('footer/mention.html.twig', [
            'controller_name' => 'FooterController',
        ]);
    }


    #[Route('/CGV', name: 'CGV')]
    public function CGV(): Response
    {
        return $this->render('footer/cgu.html.twig', [
            'controller_name' => 'FooterController',
        ]);
    }

    #[Route('/protection-donnees', name: 'protection-donnees')]
    public function protect(): Response
    {
        return $this->render('footer/protectionDonnees.html.twig', [
            'controller_name' => 'FooterController',
        ]);
    }

    #[Route('/cookies', name: 'cookies')]
    public function cookies(): Response
    {
        return $this->render('footer/cookies.html.twig', [
            'controller_name' => 'FooterController',
        ]);
    }

}
