<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ResetPassType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use mysql_xdevapi\Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        
        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/forgotten-pass", name="app-forgotten_password")
     */
    public function forgottenPass(Request $request, UserRepository $userRepository, MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator, EntityManagerInterface $entityManager)
    {
        //crée le formulaire du reset de mot de passe
        $form = $this->createForm(ResetPassType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $donnees = $form->getData();

            // on cherche si un utilisateur a cet email
            $user = $userRepository->findOneByEmail($donnees['email']);

            //si l'utilisteur n'existe pas
            if(!$user){
                // retourne un message flash
                $this->addFlash('danger', 'Cette adresse n\'existe pas');
                return $this->redirectToRoute('app_login');
            }

            $token = $tokenGenerator->generateToken();
            try {
                $user->setResetToken($token);
                $entityManager->persist($user);
                $entityManager->flush();
            }
            catch (\Doctrine\DBAL\Exception $e){
                $this->addFlash('warning', 'une erreur est survenue :'. $e->getMessage());
                return $this->redirectToRoute('app_login');
            }
            // on génère l'URL de renitialisation
            $url = $this->generateUrl('app_reset_password', [
                'token' => $token
            ], UrlGeneratorInterface::ABSOLUTE_URL);

            // envoi du message
            $message = (new TemplatedEmail())
                ->from(new Address('alternantreprise@outlook.fr', 'Contact Alternantreprise'))
                ->to($user->getEmail())
                ->subject('Rénitialiser votre mot de passe')
                ->htmlTemplate('emails/resetPass.html.twig')
                ->context([
                    'url' => $url
                ]);
            $mailer->send($message);
            $this->addFlash('message', 'une demande de mot de passe à été envoyer');

            return $this->redirect('login');
        }

        // on envoyer vers la page de demande de l'email
        return $this->render("security/forgotten_password.html.twig", ["emailForm" => $form->createView()]);
    }

    /**
     * @Route("/reset-pass/{token}", name="app_reset_password")
     */
    public function resetPassword($token, Request $request, UserPasswordHasherInterface $passwordHasher, UserRepository $userRepository, EntityManagerInterface $entityManager)
    {

        //on cherche l'utilisateur avec le token

        $user = $userRepository->findOneBy(['reset_token' => $token]);

        if (!$user) {
            $this->addFlash('danger', 'token inconnu');
            return $this->redirectToRoute('app_login');
        }

        if ($request->isMethod('POST')) {
            $user->setResetToken(null);

            $user->setPassword($passwordHasher->hashPassword($user, $request->request->get("password")));
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('message', 'le mot de passe modifié avec succès');


            return $this->redirectToRoute('app_login');
        }else {
            return $this->render('security/reset_password.html.twig', ['token' => $token]);
        }
    }
}
