<?php

namespace App\Controller;

use App\Entity\Calendrier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

class ApiController extends AbstractController
{
    /**
     * @Route("/admin/api/{id}/edit", name="api_event_edit", methods={"PUT"})
     */
    public function majEvent(?Calendrier $calendrier, Request $request, EntityManagerInterface $manager)
    {
        //recuperer les données envoyer par full Calendar
        $donnees = json_decode($request->getContent());

        if(
            isset($donnees->title) && !empty($donnees->title) &&
            isset($donnees->start) && !empty($donnees->start) &&
            isset($donnees->end) && !empty($donnees->end) &&
            isset($donnees->description) && !empty($donnees->description)
        ){
            //les données sont complète
            //on initialise un code

            $code = 200;

            // on verifie si l'id existe
            if(!$calendrier){
                $calendrier = new Calendrier();
                //on change le code

                $code = 201;
            }
            $calendrier->setTitle($donnees->title);
            $calendrier->setStart(new \DateTime($donnees->start));
            $calendrier->setEnd(new \DateTime($donnees->end));
            $calendrier->setDescription($donnees->description);

            $entityManager = $manager;
            $entityManager->persist($calendrier);
            $entityManager->flush();


            //on retorune un code
            return new Response('OK', $code);
        }
        else {
            return new Response('Données incomplètes', 404);
        }

//
//        return $this->render('api/index.html.twig', [
//            'controller_name' => 'ApiController',
//        ]);
    }
}
