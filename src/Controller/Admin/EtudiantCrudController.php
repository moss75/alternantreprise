<?php

namespace App\Controller\Admin;

use App\Entity\Etudiant;
use App\Form\CalendrierType;
use App\Repository\DiplomeRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use SebastianBergmann\CodeCoverage\Report\Text;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\DateTime;

class EtudiantCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Etudiant::class;
    }

    public function __construct(DiplomeRepository $diplomeRepository)
    {
        $this->diplomeRepository = $diplomeRepository;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('Nom'),
            TextField::new('Prenom'),
            EmailField::new('Email'),
            AssociationField::new('diplome')->setFormTypeOption('choice_label', 'libelle'),
            AssociationField::new('secteur')->setFormTypeOption('choice_label', 'libelle'),
            TextField::new('Age'),
            AssociationField::new('departement')->setFormTypeOption('choice_label', 'departement'),
            TextField::new('Telephone'),
            TextareaField::new('Observation'),
            ImageField::new('cv')->setBasePath('public/')->setUploadDir('public/cv')->setTemplatePath('/bundles/EasyAdminBundle/crud/field/cv.html.twig'),
            AssociationField::new('contrat')->setFormTypeOption('choice_label', 'type')
        ];
    }

}
