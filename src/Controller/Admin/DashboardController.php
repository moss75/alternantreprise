<?php

namespace App\Controller\Admin;

use App\Entity\Calendrier;
use App\Entity\Contrat;
use App\Entity\Diplome;
use App\Entity\Etudiant;
use App\Entity\Secteur;
use App\Entity\User;
use App\Repository\CalendrierRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DashboardController extends AbstractDashboardController
{
    public function __construct(CalendrierRepository $calendrier)
    {
        $this->calendrier = $calendrier;

    }
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $events = $this->calendrier->findAll();
        $rdvs = [];
        foreach($events as $event)
        {
            $rdvs[] = [
              'id'=> $event->getId(),
              'title'=> $event->getTitle(),
              'start'=> $event->getStart()->format('Y-m-d H:i:s'),
              'end'=> $event->getEnd()->format('Y-m-d H:i:s'),
              'description'=> $event->getDescription(),
            ];
        }
        $data = json_encode($rdvs);
         return $this->render('bundles/EasyAdminBundle/welcome.html.twig', [
             'data' => $data
         ]);
    }
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Alternantreprise');

    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Tableau de bord', 'fa fa-home');
        yield MenuItem::linktoRoute('Back to the website', 'fas fa-home', 'home');
        yield MenuItem::linkToCrud('Diplome', 'fa fa-graduation-cap', Diplome::class);
        yield MenuItem::linkToCrud('Secteur', 'fa fa-bars', Secteur::class);
        yield MenuItem::linkToCrud('Etudiant', 'fa fa-bars', Etudiant::class);
        yield MenuItem::linkToCrud('Calendrier', 'fa fa-calendar', Calendrier::class);
        yield MenuItem::linkToCrud('Contrat', 'fa fa-bars', Contrat::class);
        yield MenuItem::linkToCrud('Utilisateur', 'fa fa-bars', User::class);
    }
}
