<?php

namespace App\Controller;

use App\Entity\Etudiant;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\EtudiantRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class UsersController extends AbstractController
{
    #[Route('/users', name: 'app_users')]
    public function index(SessionInterface $session, EtudiantRepository $etudiantRepository): Response
    {
        $selection = $session->get('selection');
        $dataEtudiant = [];
        if($selection != null){
            foreach($selection as $id => $quantite){
                $etudiant = $etudiantRepository->find($id);
                $dataEtudiant[] = [
                    "id" => $etudiant->getId(),
                    "Nom" => $etudiant->getNom(),
                    "CV" => $etudiant->getCv()
                ];
            }
        }
        return $this->render('users/index.html.twig', [
            'selection' => $dataEtudiant
        ]);
    }

    #[Route('/save/{id}', name: 'save')]
    public function save(Etudiant $product, SessionInterface $session){
        $id = $product->getId();
        $selection = $session->get("selection", []);
        $selection[$id] = 1;

        $session->set("selection", $selection);
        return $this->redirectToRoute('student');
    }

    #[Route('/unsave/{id}', name: 'unsave')]
    public function unsave($id, SessionInterface $session){
        $selection = $session->get('selection', []);
        unset($selection[$id]);

        $session->set("selection", $selection);
        return $this->redirectToRoute('student');
    }

    #[Route('/unsaveUser/{id}', name: 'unsaveUser')]
    public function unsaveUser($id, SessionInterface $session){
        $selection = $session->get('selection', []);
        unset($selection[$id]);

        $session->set("selection", $selection);
        return $this->redirectToRoute('app_users');
    }

    #[Route('/description/{id}', name: 'description')]
    public function description(){

        return $this->render('users/index.html.twig', [

        ]);
    }
    #[Route('/editProfil/{id}', name: 'editProfil', methods: ['GET', 'POST'])]
    public function editProfil($id, Request $request, UserRepository $userRepository, EntityManagerInterface $entityManager, UserInterface $userInterface, User $user): Response
    {
        $userProfil = $userRepository->find($id);

        if($userProfil->getId() != null && $userProfil->getId() == $userInterface->getId()){

            $form = $this->createForm(UserType::class, $user);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();
                $entityManager->persist($user);
                $entityManager->flush();
                $this->addFlash('succes','Votre Profil à bien ete mis à jour');
                return $this->redirectToRoute('app_users');
            }
        } else {
            $this->addFlash('error','Profil inconnu');
            return $this->redirectToRoute('app_users');
        }

        return $this->render('users/edit.html.twig', [
            'userProfil' => $userProfil,
            'user' => $user,
            'form' => $form->createView(),
            'request' => $request
        ]);
    }
}
