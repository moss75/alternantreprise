<?php

namespace App\Form;

use App\Entity\Contrat;
use App\Entity\Departement;
use App\Entity\Diplome;
use App\Entity\Etudiant;
use App\Entity\Secteur;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class EtudiantFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Nom', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('Prenom', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('contrat', EntityType::class,[
                'class' => Contrat::class,
                'choice_label' => 'type',
                'required' => 'true'
        ])
            ->add('Diplome', EntityType::class, [
                'class' => Diplome::class,
                'choice_label' => 'libelle',
                'required' => true

            ])
            ->add('Secteur', EntityType::class, [
                'class' => Secteur::class,
                'choice_label' => 'libelle',
                'required' => true
            ])
            ->add('Age')
            ->add('telephone')
            ->add('observation', HiddenType::class, [

            ])
            ->add('departement', EntityType::class, [
                'class' => Departement::class,
                'choice_label'=> 'departement',
                'required' => 'true',
            ])
            ->add('email', EmailType::class, [
                'required' => 'true',
            ])
            ->add('cv', FileType::class,[
                'required' => 'true',
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                            ''
                        ],
                        'mimeTypesMessage' => 'Utiliser uniquement un document au format PDF.',
                    ])
                ]

            ])
            ->add('date', DateTimeType::class, [
                'mapped' => false,
                'widget' => 'single_text',
                'label' => ' '
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Etudiant::class,
        ]);
    }
}
