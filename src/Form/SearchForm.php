<?php

namespace App\Form;

use App\Data\SearchData;
use Symfony\Component\Form\AbstractType;
use App\Entity\Contrat;
use App\Entity\Departement;
use App\Entity\Diplome;
use App\Entity\Etudiant;
use App\Entity\Secteur;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('diplome', EntityType::class, [
                'class' => Diplome::class,
                'choice_label' => 'libelle',
                'expanded' => false,
                'multiple' => true,
                'required' => false,
            ])
            ->add('secteur', EntityType::class, [
                'class' => Secteur::class,
                'choice_label' => 'libelle',
                'expanded' => false,
                'multiple' => true,
                'required' => false,
                'attr' => [
                    'class' => 'attrOption'
                ]
            ])
            ->add('contrat', EntityType::class, [
                'class' => Contrat::class,
                'choice_label' => 'type',
                'expanded' => false,
                'multiple' => true,
                'required' => false,
                'attr' => [
                    'class' => 'attrOption'
                ]
            ])
            ->add('departement', EntityType::class, [
                'class' => Departement::class,
                'choice_label' => 'departement',
                'expanded' => false,
                'multiple' => true,
                'required' => false,
                'attr' => [
                    'class' => 'attrOption'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchData::class,
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

}