<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221207130242 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etudiant ADD date_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE etudiant ADD CONSTRAINT FK_717E22E3B897366B FOREIGN KEY (date_id) REFERENCES calendrier (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_717E22E3B897366B ON etudiant (date_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etudiant DROP FOREIGN KEY FK_717E22E3B897366B');
        $this->addSql('DROP INDEX UNIQ_717E22E3B897366B ON etudiant');
        $this->addSql('ALTER TABLE etudiant DROP date_id');
    }
}
