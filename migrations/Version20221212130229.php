<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221212130229 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO departement (departement) VALUES ("2A - Corse-du-Sud")');
        $this->addSql('INSERT INTO departement (departement) VALUES ("2B - Haute-Corse")');
        $this->addSql('INSERT INTO departement (departement) VALUES ("01 - Ain")');
        $this->addSql('INSERT INTO departement (departement) VALUES ("02 - Aisne")');
        $this->addSql('INSERT INTO departement (departement) VALUES("03 - Allier")');
        $this->addSql('INSERT INTO departement (departement) VALUES("05 - Hautes-Alpes ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("06 - Alpes-Maritimes ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("07 - Ardêche ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("08 - Ardennes ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("09 - Ariège ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("10 - Aube ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("11 - Aude ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("12 - Aveyron ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("13 - Bouches-du-Rhône ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("14 - Calvados ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("15 - Cantal ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("16 - Charente ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("17 - Charente-Maritime ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("18 - Cher ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("19 - Corrèze ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("21 - Côte-d\'Or ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("22 - Côtes d\'Armor ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("23 - Creuse ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("24 - Dordogne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("25 - Doubs ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("26 - Drôme ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("27 - Eure ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("28 - Eure-et-Loir ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("29 - Finistère ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("30 - Gard ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("31 - Haute-Garonne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("32 - Gers ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("33 - Gironde ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("34 - Hérault ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("35 - Île-et-Vilaine ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("36 - Indre ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("37 - Indre-et-Loire")');
        $this->addSql('INSERT INTO departement (departement) VALUES("38 - Isère ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("39 - Jura ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("40 - Landes ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("41 - Loir-et-Cher ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("42 - Loire ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("43 - Haute-Loire ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("44 - Loire-Atlantique ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("45 - Loiret ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("46 - Lot ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("47 - Lot-et-Garonne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("48 - Lozère ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("49 - Maine-et-Loire ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("50 - Manche ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("51 - Marne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("52 - Haute-Marne")');
        $this->addSql('INSERT INTO departement (departement) VALUES("53 - Mayenne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("54 - Meurthe-et-Moselle ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("55 - Meuse ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("56 - Morbihan ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("57 - Moselle ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("58 - Nièvre ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("59 - Nord ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("60 - Oise ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("61 - Orne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("62 - Pas-de-Calais ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("63 - Puy-de-Dôme ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("64 - Pyrénées-Atlantiques ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("65 - Hautes-Pyrénées ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("66 - Pyrénées-Orientales ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("67 - Bas-Rhin ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("68 - Haut-Rhin ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("69 - Rhône ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("70 - Haute-Saône ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("71 - Saône-et-Loire ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("72 - Sarthe ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("73 - Savoie ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("74 - Haute-Savoie ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("75 - Paris ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("76 - Seine-Maritime ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("77 - Seine-et-Marne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("78 - Yvelines ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("79 - Deux-Sèvres ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("80 - Somme ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("81 - Tarn ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("82 - Tarn-et-Garonne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("83 - Var ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("84 - Vaucluse ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("85 - Vendée ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("86 - Vienne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("87  - Haute-Vienne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("88  - Vosges ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("89  - Yonne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("90 - Territoire-de-Belfort ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("91 - Essonne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("92 - Hauts-de-Seine ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("93 - Seine-Saint-Denis ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("94 - Val-de-Marne ")');
        $this->addSql('INSERT INTO departement (departement) VALUES("95 - Val-d\'Oise ")');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM departement WHERE departement = "2A - Corse-du-Sud "');
        $this->addSql('DELETE FROM departement WHERE departement = "2B - Haute-Corse "');
        $this->addSql('DELETE FROM departement WHERE departement = "01 - Ain"');
        $this->addSql('DELETE FROM departement WHERE departement = "02 - Aisne"');
        $this->addSql('DELETE FROM departement WHERE departement = "03 - Allier"');
        $this->addSql('DELETE FROM departement WHERE departement = "05 - Hautes-Alpes "');
        $this->addSql('DELETE FROM departement WHERE departement = "06 - Alpes-Maritimes "');
        $this->addSql('DELETE FROM departement WHERE departement = "07 - Ardêche "');
        $this->addSql('DELETE FROM departement WHERE departement = "08 - Ardennes "');
        $this->addSql('DELETE FROM departement WHERE departement = "09 - Ariège "');
        $this->addSql('DELETE FROM departement WHERE departement = "10 - Aube "');
        $this->addSql('DELETE FROM departement WHERE departement = "11 - Aude "');
        $this->addSql('DELETE FROM departement WHERE departement = "12 - Aveyron "');
        $this->addSql('DELETE FROM departement WHERE departement = "13 - Bouches-du-Rhône "');
        $this->addSql('DELETE FROM departement WHERE departement = "14 - Calvados "');
        $this->addSql('DELETE FROM departement WHERE departement = "15 - Cantal "');
        $this->addSql('DELETE FROM departement WHERE departement = "16 - Charente "');
        $this->addSql('DELETE FROM departement WHERE departement = "17 - Charente-Maritime "');
        $this->addSql('DELETE FROM departement WHERE departement = "18 - Cher "');
        $this->addSql('DELETE FROM departement WHERE departement = "19 - Corrèze "');
        $this->addSql('DELETE FROM departement WHERE departement = "21 - Côte-d\'Or "');
        $this->addSql('DELETE FROM departement WHERE departement = "22 - Côtes d\'Armor "');
        $this->addSql('DELETE FROM departement WHERE departement = "23 - Creuse "');
        $this->addSql('DELETE FROM departement WHERE departement = "24 - Dordogne "');
        $this->addSql('DELETE FROM departement WHERE departement = "25 - Doubs "');
        $this->addSql('DELETE FROM departement WHERE departement = "26 - Drôme "');
        $this->addSql('DELETE FROM departement WHERE departement = "27 - Eure "');
        $this->addSql('DELETE FROM departement WHERE departement = "28 - Eure-et-Loir "');
        $this->addSql('DELETE FROM departement WHERE departement = "29 - Finistère "');
        $this->addSql('DELETE FROM departement WHERE departement = "30 - Gard "');
        $this->addSql('DELETE FROM departement WHERE departement = "31 - Haute-Garonne "');
        $this->addSql('DELETE FROM departement WHERE departement = "32 - Gers "');
        $this->addSql('DELETE FROM departement WHERE departement = "33 - Gironde "');
        $this->addSql('DELETE FROM departement WHERE departement = "34 - Hérault "');
        $this->addSql('DELETE FROM departement WHERE departement = "35 - Île-et-Vilaine "');
        $this->addSql('DELETE FROM departement WHERE departement = "36 - Indre "');
        $this->addSql('DELETE FROM departement WHERE departement = "37 - Indre-et-Loire"');
        $this->addSql('DELETE FROM departement WHERE departement = "38 - Isère "');
        $this->addSql('DELETE FROM departement WHERE departement = "39 - Jura "');
        $this->addSql('DELETE FROM departement WHERE departement = "40 - Landes "');
        $this->addSql('DELETE FROM departement WHERE departement = "41 - Loir-et-Cher "');
        $this->addSql('DELETE FROM departement WHERE departement = "42 - Loire "');
        $this->addSql('DELETE FROM departement WHERE departement = "43 - Haute-Loire "');
        $this->addSql('DELETE FROM departement WHERE departement = "44 - Loire-Atlantique "');
        $this->addSql('DELETE FROM departement WHERE departement = "45 - Loiret "');
        $this->addSql('DELETE FROM departement WHERE departement = "46 - Lot "');
        $this->addSql('DELETE FROM departement WHERE departement = "47 - Lot-et-Garonne "');
        $this->addSql('DELETE FROM departement WHERE departement = "48 - Lozère "');
        $this->addSql('DELETE FROM departement WHERE departement = "49 - Maine-et-Loire "');
        $this->addSql('DELETE FROM departement WHERE departement = "50 - Manche "');
        $this->addSql('DELETE FROM departement WHERE departement = "51 - Marne "');
        $this->addSql('DELETE FROM departement WHERE departement = "52 - Haute-Marne"');
        $this->addSql('DELETE FROM departement WHERE departement = "53 - Mayenne "');
        $this->addSql('DELETE FROM departement WHERE departement = "54 - Meurthe-et-Moselle "');
        $this->addSql('DELETE FROM departement WHERE departement = "55 - Meuse "');
        $this->addSql('DELETE FROM departement WHERE departement = "56 - Morbihan "');
        $this->addSql('DELETE FROM departement WHERE departement = "57 - Moselle "');
        $this->addSql('DELETE FROM departement WHERE departement = "58 - Nièvre "');
        $this->addSql('DELETE FROM departement WHERE departement = "59 - Nord "');
        $this->addSql('DELETE FROM departement WHERE departement = "60 - Oise "');
        $this->addSql('DELETE FROM departement WHERE departement = "61 - Orne "');
        $this->addSql('DELETE FROM departement WHERE departement = "62 - Pas-de-Calais "');
        $this->addSql('DELETE FROM departement WHERE departement = "63 - Puy-de-Dôme "');
        $this->addSql('DELETE FROM departement WHERE departement = "64 - Pyrénées-Atlantiques "');
        $this->addSql('DELETE FROM departement WHERE departement = "65 - Hautes-Pyrénées "');
        $this->addSql('DELETE FROM departement WHERE departement = "66 - Pyrénées-Orientales "');
        $this->addSql('DELETE FROM departement WHERE departement = "67 - Bas-Rhin "');
        $this->addSql('DELETE FROM departement WHERE departement = "68 - Haut-Rhin "');
        $this->addSql('DELETE FROM departement WHERE departement = "69 - Rhône "');
        $this->addSql('DELETE FROM departement WHERE departement = "70 - Haute-Saône "');
        $this->addSql('DELETE FROM departement WHERE departement = "71 - Saône-et-Loire "');
        $this->addSql('DELETE FROM departement WHERE departement = "72 - Sarthe "');
        $this->addSql('DELETE FROM departement WHERE departement = "73 - Savoie "');
        $this->addSql('DELETE FROM departement WHERE departement = "74 - Haute-Savoie "');
        $this->addSql('DELETE FROM departement WHERE departement = "75 - Paris "');
        $this->addSql('DELETE FROM departement WHERE departement = "76 - Seine-Maritime "');
        $this->addSql('DELETE FROM departement WHERE departement = "77 - Seine-et-Marne "');
        $this->addSql('DELETE FROM departement WHERE departement = "78 - Yvelines "');
        $this->addSql('DELETE FROM departement WHERE departement = "79 - Deux-Sèvres "');
        $this->addSql('DELETE FROM departement WHERE departement = "80 - Somme "');
        $this->addSql('DELETE FROM departement WHERE departement = "81 - Tarn "');
        $this->addSql('DELETE FROM departement WHERE departement = "82 - Tarn-et-Garonne "');
        $this->addSql('DELETE FROM departement WHERE departement = "83 - Var "');
        $this->addSql('DELETE FROM departement WHERE departement = "84 - Vaucluse "');
        $this->addSql('DELETE FROM departement WHERE departement = "85 - Vendée "');
        $this->addSql('DELETE FROM departement WHERE departement = "86 - Vienne "');
        $this->addSql('DELETE FROM departement WHERE departement = "87  - Haute-Vienne "');
        $this->addSql('DELETE FROM departement WHERE departement = "88  - Vosges "');
        $this->addSql('DELETE FROM departement WHERE departement = "89  - Yonne "');
        $this->addSql('DELETE FROM departement WHERE departement = "90 - Territoire-de-Belfort "');
        $this->addSql('DELETE FROM departement WHERE departement = "91 - Essonne "');
        $this->addSql('DELETE FROM departement WHERE departement = "92 - Hauts-de-Seine "');
        $this->addSql('DELETE FROM departement WHERE departement = "93 - Seine-Saint-Denis "');
        $this->addSql('DELETE FROM departement WHERE departement = "94 - Val-de-Marne "');
        $this->addSql('DELETE FROM departement WHERE departement = "95 - Val-d\'Oise "');

    }
}
